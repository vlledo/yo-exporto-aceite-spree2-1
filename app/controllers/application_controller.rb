class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  before_action :first_time_visiting?
  protect_from_forgery with: :exception

  def first_time_visiting?
    if session[:first_time].nil?
    	session[:first_time] = 1
    else
    	session[:first_time] = 2
    end
  end
end
